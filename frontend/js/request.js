const API_URL = "/api/";

export async function request(route, parameters = {}) {
    const method = parameters.hasOwnProperty("method") ? parameters["method"] : "GET";
    const body = parameters.hasOwnProperty("body") ? parameters["body"] : null;
    const accept = parameters.hasOwnProperty("accept") ? parameters["accept"] : "application/json";
    const contentType = parameters.hasOwnProperty("contentType") ? parameters["contentType"] : "application/json";
    const parseResponse = parameters.hasOwnProperty("parseResponse") ? parameters["parseResponse"] : "json";
    const url = `${API_URL}${route}`;

    const headers = new Headers();
    if (accept) headers.append("Accept", accept);
    if (contentType) headers.append("Content-Type", contentType);

    parameters = {
        method,
        // mode: "cors",
        headers,
    };
    if (body)
        parameters["body"] =
            body instanceof FormData || contentType !== "application/json" ? body : JSON.stringify(body);

    const response = await fetch(url, parameters);

    if (response.ok)
        if (parseResponse === "json") return await response.json();
        else if (parseResponse === "blob") {
            await downloadBlob(response, accept);

            return {
                status: "success",
            };
        } else return;

    console.error(`Request ${route} failed with status: ${response.status}`);
}
