import Vue from "vue";
import App from "./App";


// Initialization of VueJS in the #app div
new Vue({
    el: "#app",
    render: h => h(App),
});
