from datetime import datetime
from io import BytesIO
from pathlib import Path
import requests
from typing import Optional

from PIL import Image

WAVELENGTH_TO_SOURCEID = {
    "94": 8,
    "131": 9,
    "171": 10,
    "193": 11,
    "211": 12,
    "304": 13,
    "335": 14,
    "1600": 15,
    "1700": 16,
    "4500": 17,
    "continuum": 18,
    "magnetogram": 19,
}

BASE_API_URL = "https://api.helioviewer.org/v2"
BACKUP_API_URL = "https://helioviewer-api.ias.u-psud.fr//v2"
GET_JP2_IMAGE_URL = "{}/getJP2Image/?date={}&sourceId={}"
GET_CLOSEST_IMAGE_DATA_URL = "{}/getClosestImage/?date={}&sourceId={}"


def get_image(date: datetime, wavelength: str = "magnetogram", save_path: Optional[Path] = None):
    if save_path is not None and save_path.exists():
        return Image.open(save_path)

    url = GET_JP2_IMAGE_URL.format(BASE_API_URL, date.isoformat() + "Z", WAVELENGTH_TO_SOURCEID[wavelength])
    response = requests.get(url)
    img = Image.open(BytesIO(response.content))

    if save_path is not None:
        save_path.parent.mkdir(parents=True, exist_ok=True)
        img.save(save_path)
        save_path_1024 = save_path.parent / (save_path.stem + "_1024" + save_path.suffix)
        img.resize((1024, 1024), Image.LANCZOS).save(save_path_1024)

    return img


def get_closest_image_data(date: datetime, wavelength: str = "magnetogram"):
    url = GET_CLOSEST_IMAGE_DATA_URL.format(BASE_API_URL, date.isoformat() + "Z", WAVELENGTH_TO_SOURCEID[wavelength])
    response = requests.get(url)

    return response.json()
