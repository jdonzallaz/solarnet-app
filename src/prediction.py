import numpy as np
import torch
from PIL import Image
from solarnet.models import ImageClassification
from torchvision import transforms
import torch.nn.functional as F


def create_circular_mask(h, w, center=None, radius=None):
    if center is None:  # use the middle of the image
        center = (int(w / 2), int(h / 2))
    if radius is None:  # use the smallest distance between the center and image walls
        radius = min(center[0], center[1], w - center[0], h - center[1])

    Y, X = np.ogrid[:h, :w]
    dist_from_center = np.sqrt((X - center[0]) ** 2 + (Y - center[1]) ** 2)

    mask = dist_from_center <= radius
    return mask


def preprocess_image(img: Image.Image, resize: int = 128):
    size = img.size[0]

    # Remove black background around the sun in jpg images

    # hd.RSUN_OBS/hd.CDELT1 = 1625 -> 1625*2=3250 -> 3250 / 4096 = 0.79... --> RSUN_OBS/CDELT1 / 4096 =~ 0.3970
    # TODO: use proper value from FITS file
    mask = create_circular_mask(size, size, center=(size // 2, size // 2), radius=size * 0.3955)

    def remove_background(x):
        x_copy = x.clone()
        if len(x.shape) == 3:
            x_copy[0][~mask] = 0.5
        else:
            x_copy[~mask] = 0.5
        return x_copy

    # Downsizing by averaging in local blocks
    target_mean_size = max(512, resize)
    factor = size // target_mean_size
    mean_downsize = lambda x: torch.nn.functional.avg_pool2d(x, factor)

    transform = transforms.Compose(
        [
            transforms.ToTensor(),
            transforms.Lambda(remove_background),
            transforms.Lambda(mean_downsize),
            transforms.CenterCrop((target_mean_size // 2, target_mean_size - target_mean_size // 8)),
            transforms.Resize(resize),
            transforms.Normalize(mean=[0.5], std=[0.5]),
        ]
    )
    return transform(img)


def get_prediction(img: Image.Image) -> (str, float):
    # Load model
    model = ImageClassification.from_pretrained("solarnet-ssl-bz-ft-month")

    # Preprocess image
    tensor = preprocess_image(img, resize=128)

    while len(tensor.shape) < 4:
        tensor = tensor.unsqueeze(0)

    # Predict
    with torch.no_grad():
        y_pred = model(tensor)

    # Postprocess
    y_pred = y_pred[0]
    y_pred = F.softmax(y_pred, dim=0)
    confidence, y_pred = torch.max(y_pred, 0)
    confidence = confidence.item()
    y_pred = y_pred.item()
    y_pred = ["no-flare", "flare"][y_pred]

    return y_pred, confidence
