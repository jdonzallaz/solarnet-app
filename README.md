# SolarNet App

This application serves a model and makes live predictions of flares
at [solarnet.kube.isc.heia-fr.ch](https://solarnet.kube.isc.heia-fr.ch). It uses
the [Solarnet library](https://gitlab.com/jdonzallaz/solarnet)
([docs](https://jdonzallaz.gitlab.io/solarnet/)) and
the [SSL model finetuned on SDO-Dataset "month"](https://jdonzallaz.gitlab.io/solarnet/models/solarnet-ssl-bz-ft-month/)
.

## Development

Requires Python 3.9 and Node.js.

### Install dependencies

```bash
# Python environment
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt

# Node environment
npm i
```

### Run application

Run in two consoles.

```bash
uvicorn app:app

npm run watch
```

## Deployment

Building and deployment is managed by CI/CD with Gitlab-CI. The `.gitlab-ci.yml` defines those steps. A Docker image is
created and deployed on our Kubernetes cluster.

## Authors

The SolarNet application is developed by [Jonathan Donzallaz](mailto:jonathan.donzallaz@hefr.ch), researcher at iCoSys
institute, HEIA-FR, HES-SO. Thanks to Jean Hennebert, iCoSys, and André Csillaghy, FHNW, for their contributions.

## License

This project is licensed under the terms of the MIT license.
