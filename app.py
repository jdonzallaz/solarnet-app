import logging
import re
import traceback
from datetime import datetime
from pathlib import Path

from fastapi import FastAPI, HTTPException
from fastapi.staticfiles import StaticFiles
from fastapi_camelcase import CamelModel
from pydantic import Field
from starlette.middleware.cors import CORSMiddleware
from starlette.requests import Request
from starlette.responses import HTMLResponse
from starlette.templating import Jinja2Templates

from src.downloader import get_closest_image_data, get_image
from src.prediction import get_prediction

app = FastAPI(
    title="SolarNet API",
    description="SolarNet API lets you forecast solar flares.",
    version="1.0.0",
    contact={
        "name": "Jonathan Donzallaz",
        "email": "jonathan.donzallaz@hefr.ch",
    },
    license_info={
        "name": "MIT",
        "url": "https://mit-license.org/",
    },
)
templates = Jinja2Templates(directory="static")
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
app.mount("/static", StaticFiles(directory="static"), name="static")

# Logging
logger = logging.getLogger("uvicorn.error")
logger.setLevel(logging.INFO)
logger.info("SolarNet is starting...")


class PredictQuery(CamelModel):
    date: str = Field(..., example="2022-03-03T18:17:37")


class PredictResponse(CamelModel):
    prediction: str = Field(..., example="flare")
    date: str = Field(..., example="2022-03-03T18:18:20")
    confidence: float = Field(..., example=0.82)


@app.post("/api/predict", response_model=PredictResponse)
def predict(predict_query: PredictQuery):
    date = datetime.strptime(predict_query.date, "%Y-%m-%dT%H:%M:%S")

    try:
        img_data = get_closest_image_data(date)
        found_date = datetime.strptime(img_data["date"], "%Y-%m-%d %H:%M:%S")

        date_filtered = re.sub("[-:]", "", found_date.isoformat())
        path_str = f"static/images/HMI_{date_filtered}.jpg"

        img = get_image(date, save_path=Path(path_str))

        y_pred, confidence = get_prediction(img)
    except Exception as e:
        logger.error(f"Error while predicting with input: {predict_query}")
        logger.error(e)
        traceback.print_exception(type(e), e, e.__traceback__)
        raise HTTPException(status_code=500, detail="An error occurred while processing the data.")

    return {
        "date": found_date.isoformat(),
        "prediction": y_pred,
        "confidence": confidence,
    }


@app.get("/", response_class=HTMLResponse)
def frontend(request: Request):
    template_params = {
        "request": request,
    }

    return templates.TemplateResponse("index.html", template_params)
