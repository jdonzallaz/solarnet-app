# --- Stage 0, "build-stage", based on Node.js, to build and compile the frontend
FROM node:14 as build-stage

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY ./ ./

RUN npm run build
RUN ls -al


# --- Stage 1, run app, use an light Python runtime as a parent image
FROM python:3.8.11-slim

# Set the working directory to /app
WORKDIR /app

# Copy the requirements file into the container at /app
COPY ./requirements.txt ./requirements.txt

# Upgrade pip if necessary
RUN pip install --no-cache-dir --upgrade pip

# Install dependencies
RUN pip install --no-cache-dir --trusted-host pypi.python.org -r requirements.txt

# Copy artifacts from build
COPY --from=build-stage /app/static/ ./static/

# Copy the current directory contents into the container at /app
COPY ./ ./

# Configure exposed port
ENV PORT=80
ENV HOST=0.0.0.0

# Run the app
ENTRYPOINT uvicorn app:app --host $HOST --port $PORT --access-log --log-level info --use-colors
